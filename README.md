# Atlassian Cloud Carousel #

![Preview of the Atlassian Cloud Carousel](https://bitbucket.org/delvarworld/jquery-cloud-carousel/raw/9461f3221256/cloud-carousel.png)

Turn an element with child images in containers into an image carousel. Built
for 3 images or more. Will do horrible things if less than 3 images provided.

Suggested markup:

    <ul id="carousel">
        <li class="carousel-item-wrap">
            <a href="#" class="carousel-item">
                <img src="your_banner.jpg">
            </a>
        </li>
        <li class="carousel-item-wrap">
            <a href="#" class="carousel-item">
                <img src="your_banner_1.jpg">
            </a>
        </li>
        ...
    </ul>

# Usage #

    $jqueryObject.cloudCarousel(options)

# Options #

    selector
        The CSS selector to find the containers holding the images
        Default: '.carousel-item-wrap'

    width
        The total width of the carousel including side images and padding
        Default: 745

    height
        The total height of the carousel including padding. The main container
            will size to this height to reserve the room on the page.
        Default: 237

    centerWidth
        The width of the center image, not including padding
        Default: 512

    centerHeight
        The height of the center image not including padding. The total height
            of the carousel will be centerHeight + containerPadding
            Will also be height of center image.
        Default: 225

    scaledWidth
        The width of a side image, not including padding
        Default: 405

    scaledHeight
        The height of a side image, not including padding
        Default: 178

    animateTime
        How long the images take to rotate, in milliseconds
        Default: 500

    animated
        If we should even animate at all?!
        Default: true

    containerPadding
        The padding (outerWidth - width) of the center image. If you have 5px padding
            with a 2px border, you should set this to 14 (5 + 5 + 2 + 2)
        Default: 14

    sideOpacity
        The opacity to fade each side image to when not in focus. Not used in IE7
        Default: 0.2

# Tests #

A jQuery plugin with tests? Say what?

Just open `test/tests.html` in a browser. It runs with qUnit. Actual tests live in `test/tests/tests.js`
