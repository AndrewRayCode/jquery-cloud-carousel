var util = {
    makeCarousel: function(lengthOrImages) {
        var i = 0, x = 0, arr = [], image,
            $carousel = $('<ul class="cloud-carousel"></ul>');

        if (!lengthOrImages.length) {
            for(; x++ < lengthOrImages;) {
                arr.push('stormySea.jpg');
            }
        } else {
            arr = lengthOrImages;
        }

        for(; image = arr[i++]; ) {
            $carousel.append(
                $('<li class="carousel-item-wrap"><a href="#" class="carousel-item"><img src="' +
                    (!image.indexOf('http:') ? image : 'resources/' + image) + '"></a></li>')
            );
        }
        return $carousel.cloudCarousel();
    },
    makeLiveCarousel: function(size, callback) {
        var $carousel = this.makeCarousel(size).appendTo(document.body).bind('loaded', function() {
            callback($carousel);
        });
    },
    liveTest: function(name, count, test, noSync) {
        if(!test) {
            test = count;
            count = 3;
        }
        asyncTest(name, function() {
            util.makeLiveCarousel(count, function($carousel) {
                test($carousel);
                if (!noSync) {
                    util.done($carousel);
                }
            });
        });
    },
    liveTestWithAsync: function(name, count, test) {
        this.liveTest(name, count, test, true);
    },
    done: function($carousel) {
        if (util.destroy) {
            $carousel.remove();
        }
        start();
    }
};

$.fn.goRightBy = function(count, callback) {
    var rotated = 0,
        $carousel = this,
        bound = function() {
            if (++rotated === count) {
                $carousel.unbind('rotated', bound);
                // Make sure it actually rotated
                callback();
            } else {
                $carousel.rotateRight();
            }
        };
    $carousel.bind('rotated', bound);
    $carousel.rotateRight();
};

$.fn.getSliderContainer = function(position) {
    return this.getSlider(position).$container;
};

$.fn.getSliderImage = function(position) {
    return this.getSlider(position).$img;
};
