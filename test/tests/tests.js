// Remove carousels when done with test
util.destroy = true;

$(document).ready(function() {
    test('rotate right goes to next index', function() {
        equal(util.makeCarousel(3).rotateRight().index, 1);
    });

    test('rotate left goes to last index', function() {
        equal(util.makeCarousel(3).rotateLeft().index, 2);
    });

    util.liveTest('tall image scaled properly', ['tall-image.jpg', 'tall-image.jpg', 'tall-image.jpg'], function($carousel) {
        expect(3);

        equal($carousel.getSliderImage('left').css('height'), '178px');
        equal($carousel.getSliderImage('center').css('height'), '225px');
        equal($carousel.getSliderImage('right').css('height'), '178px');
    });

    util.liveTest('skinny image scaled properly', ['skinny-image.jpg', 'skinny-image.jpg', 'skinny-image.jpg'], function($carousel) {
        expect(3);

        equal($carousel.getSliderImage('left').css('width'), '364px');
        equal($carousel.getSliderImage('center').css('width'), '460px');
        equal($carousel.getSliderImage('right').css('width'), '364px');
    });

    util.liveTest('right image floated right', function($carousel) {
        expect(1);
        equal($carousel.getSliderContainer(1).css('left'), '367px');
    });

    util.liveTest('left image floated right', function($carousel) {
        expect(1);
        equal($carousel.getSliderContainer(2).css('left'), '0px');
    });

    util.liveTest('test two images are turned into four', 2, function($carousel) {
        expect(1);
        ok($carousel.getSliderContainer(3));
    });

    util.liveTest('test one image is only one image', 1, function($carousel) {
        expect(2);
        try {
            $carousel.getSliderContainer(1);
            equal(true, false, 'bad, there was a slider 1');
        } catch(e) {
            ok('good, there was no slider 1');
            ok($carousel.getSliderContainer(0));
        }
    });

    util.liveTestWithAsync('center image from right is centered', 3, function($carousel) {
        expect(1);
        $carousel.goRightBy(1, function() {
            equal($carousel.getSlider('center').$img.css('left'), '80px');
            util.done($carousel);
        });
    });

    util.liveTestWithAsync('center image from back is centered', 4, function($carousel) {
        expect(1);
        $carousel.goRightBy(2, function() {
            equal($carousel.getSlider('center').$img.css('left'), '80px');
            util.done($carousel);
        });
    });

    util.liveTestWithAsync('non cached image loads properly', [
            // If my server goes down (likely) or this is still being used when
            // that happens (unlikely), this image is 3733 x 2800 and about 1.5mb
            'http://andrewray.me/stuff/huge-image.jpg?' + new Date().getTime(),
            'http://andrewray.me/stuff/huge-image.jpg?' + new Date().getTime(),
            'http://andrewray.me/stuff/huge-image.jpg?' + new Date().getTime()
        ], function($carousel) {
        expect(3);
        // the bug that warranted this test had images loading not-centered, so
        // make sure all the lefts are where they should be.
        equal($carousel.getSliderImage('left').css('left'), '0px');
        equal($carousel.getSliderImage('center').css('left'), '80px');
        equal($carousel.getSliderImage('right').css('left'), '126px');
        util.done($carousel);
    });
});
